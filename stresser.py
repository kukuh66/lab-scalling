#!/usr/bin/env python3
"""
A simple script for stress testing a Web service.

Requirements:

- Python 3.7, or a Python version that already includes f-string syntax and
namedtuple in collections module
- `requests` library. Get it via `pip install requests`
"""
from collections import namedtuple
from concurrent.futures import as_completed, ThreadPoolExecutor
import os
import requests

# Silakan diganti dengan URL Web service yang Anda deploy
ENDPOINT_URL = 'https://web-calculator-api-dev.azurewebsites.net/'
CPU_CORES = os.cpu_count()

Result = namedtuple('Result', ['number', 'success', 'output'],
                    defaults=[False, dict()])
SUCCESS_JOBS, FAILED_JOBS = [], []


class FailedJobException(Exception):
    """Represents a failed job execution."""

    def __init__(self, result):
        super().__init__(self)
        self.result = result


def compute_fibonacci(number: int) -> Result:
    """Computes the n-th Fibonacci number via WS-based RPC."""
    response = requests.get(f'{ENDPOINT_URL}/calculator/fibonacci/{number}')

    if not response.ok:
        result = Result(number, False)
        raise FailedJobException(result)
    else:
        json = response.json()
        result = Result(number, True, json)
        return result


def main():
    """Executes the main program."""
    from random import choice
    with ThreadPoolExecutor(max_workers=CPU_CORES * 2) as executor:
        jobs = [executor.submit(compute_fibonacci, choice(range(10, 45)))
                for _ in range(50)]
        _set_done_callback(jobs)

        for job in as_completed(jobs):
            try:
                result = job.result()
                print(f'Finished computing {result.number}th Fibonacci number')
                print(f'Took {result.output["elapsedTime"]} milisecond(s)')
            except FailedJobException as ex:
                print(ex)

    print(f'Number of success jobs: {len(SUCCESS_JOBS)}')
    print(f'Number of failed jobs: {len(FAILED_JOBS)}')


def _set_done_callback(jobs: list):
    for job in jobs:
        job.add_done_callback(lambda f: SUCCESS_JOBS.append(f.result())
                              if f.done()
                              else FAILED_JOBS.append(f.exception()))


if __name__ == '__main__':
    main()
