package id.ac.ui.cs.digitalent.handson.calcws.controller;

import id.ac.ui.cs.digitalent.handson.calcws.controller.HelloController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(HelloController.class)
@AutoConfigureRestDocs
public class HelloControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        mockMvc.perform(get("/hello"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("42")))
                .andExpect(content().string(containsString("Hello, World")))
                .andDo(document("hello", responseFields(
                        fieldWithPath("requestTime").description("Timestamp when request was received"),
                        fieldWithPath("responseTime").description("Timestamp when response is ready"),
                        fieldWithPath("elapsedTime").description("Elapsed time"),
                        fieldWithPath("output.message").description("The welcome message for the user"),
                        fieldWithPath("output.answer").description("Answer to the Ultimate Question of Life, the Universe, and Everything")))
                );
    }
}