package id.ac.ui.cs.digitalent.handson.calcws.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CalculatorController.class)
@AutoConfigureRestDocs
public class CalculatorControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void isEven_givenValidParam_shouldReturnResultJSON() throws Exception {
        mockMvc.perform(get("/calculator/is_even/4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("true")))
                .andDo(document("calculator/is_even", responseFields(
                        fieldWithPath("requestTime").description("Timestamp when request was received"),
                        fieldWithPath("responseTime").description("Timestamp when response is ready"),
                        fieldWithPath("elapsedTime").description("Elapsed time"),
                        fieldWithPath("output.result").description("Output")
                )));
    }

    @Test
    public void isEven_givenEmptyParam_shouldReturnNotFound() throws Exception {
        mockMvc.perform(get("/calculator/is_even"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void isEven_givenInvalidParam_shouldReturnBadRequest() throws Exception {
        mockMvc.perform(get("/calculator/is_even/asdfg"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void isOdd_givenValidParam_shouldReturnResultJSON() throws Exception {
        mockMvc.perform(get("/calculator/is_odd/3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("true")))
                .andDo(document("calculator/is_odd", responseFields(
                    fieldWithPath("requestTime").description("Timestamp when request was received"),
                    fieldWithPath("responseTime").description("Timestamp when response is ready"),
                    fieldWithPath("elapsedTime").description("Elapsed time"),
                    fieldWithPath("output.result").description("Output")
                )));
    }

    @Test
    public void isOdd_givenInvalidParam_shouldReturnBadRequest() throws Exception {
        mockMvc.perform(get("/calculator/is_odd/asdfg"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void isOdd_givenEmptyParam_shouldReturnNotFound() throws Exception {
        mockMvc.perform(get("/calculator/is_odd"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void computeRectangleArea_givenValidParam_shouldReturnResultJSON() throws Exception {
        mockMvc.perform(get("/calculator/rectangle_area/3/5"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("15")))
                .andDo(document("calculator/is_odd", responseFields(
                    fieldWithPath("requestTime").description("Timestamp when request was received"),
                    fieldWithPath("responseTime").description("Timestamp when response is ready"),
                    fieldWithPath("elapsedTime").description("Elapsed time"),
                    fieldWithPath("output.result").description("Area of the rectangle")
                )));
    }

    @Test
    public void computeRectangleArea_givenInvalidParam_shouldReturnBadRequest() throws Exception {
        mockMvc.perform(get("/calculator/rectangle_area/lol/wut"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void computeRectangleArea_givenEmptyParam_shouldReturnNotFound() throws Exception {
        mockMvc.perform(get("/calculator/rectangle_area"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void computeFibonacci_givenValidParam_shouldReturnResultJSON() throws Exception {
        mockMvc.perform(get("/calculator/fibonacci/5"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("5")))
                .andDo(document("calculator/is_odd", responseFields(
                    fieldWithPath("requestTime").description("Timestamp when request was received"),
                    fieldWithPath("responseTime").description("Timestamp when response is ready"),
                    fieldWithPath("elapsedTime").description("Elapsed time"),
                    fieldWithPath("output.result").description("Computed Fibonacci number")
                )));
    }

    @Test
    public void computeFibonacci_givenInvalidParam_shouldReturnBadRequest() throws Exception {
        mockMvc.perform(get("/calculator/fibonacci/asdfg"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void computeFibonacci_givenEmptyParam_shouldReturnNotFound() throws Exception {
        mockMvc.perform(get("/calculator/fibonacci"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}