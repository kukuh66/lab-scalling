package id.ac.ui.cs.digitalent.handson.calcws.util;

import id.ac.ui.cs.digitalent.handson.calcws.model.Output;

import java.time.Instant;
import java.util.Map;

public class ResponseHelper {

    private ResponseHelper() {
        // Default, private constructor
    }

    public static Output prepareOutput(Instant start,
                                       Map<String, Object> payload) {
        return new Output(start.toEpochMilli(), Instant.now().toEpochMilli(),
                payload);
    }
}
