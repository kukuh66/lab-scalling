package id.ac.ui.cs.digitalent.handson.calcws.model;

import lombok.Data;

import java.util.Map;

@Data
public class Output {

    private final Long requestTime;
    private final Long responseTime;
    private final Map<String, Object> output;

    public final Object insert(String key, Object value) {
        return output.put(key, value);
    }

    public final Long getElapsedTime() {
        return responseTime - requestTime;
    }
}
